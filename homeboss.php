<?php
session_start();
// Nos aseguramos de que haya un usuario logueado
if (isset($_SESSION["chef"])) {
    // Nos aseguramos que el usuario logueado sea jefe
    if ($_SESSION["category"] == "Jefe") {
        ?>

        <!DOCTYPE html>
        <!--
        To change this license header, choose License Headers in Project Properties.
        To change this template file, choose Tools | Templates
        and open the template in the editor.
        -->
        <html>
            <head>
                <meta charset="UTF-8">
                <title>Pagina del Patrón</title>
            </head>
            <body>
                <h1>Bienvenido Patrón</h1>
                <?php
                ?>
            </body>
        </html>

        <?php
    } else {
        echo "Eres un chef, no un patrón";
    }
} else {
    echo "No hay ningún usuario logueado.";
}
?>
