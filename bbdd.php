<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Función que recibe un id y devuelve su category
function getCategoryById($idchef) {
    $c = conectar();
    $select = "select category from chef where idchef = $idchef";
    $resultado = mysqli_query($c, $select);
    $fila = mysqli_fetch_assoc($resultado);
    extract($fila);
    desconectar($c);
//    return $fila["category"];
    return $category;
}


// función que recibe un idchef y un código y devuelve true si es correcto
// false en caso contrario
function comprobacion($idchef, $code) {
    $c = conectar();
    $select = "select idchef, code from chef where idchef=$idchef and code=$code";
    $resultado = mysqli_query($c, $select);
    desconectar($c);
//    if (mysqli_num_rows($resultado) == 1) {
//        return true;
//    } else {
//        return false;
//    }
    // Versión optimizada del if anterior
    return mysqli_num_rows($resultado) == 1;
}

// Función que cierra una conexión
function desconectar($conexion) {
    mysqli_close($conexion);
}

// Función que se conecta a una base de datos (school)
function conectar() {
    // conectamos a la bbdd (nos devuelve una conexión)
    $conexion = mysqli_connect("localhost", "root", "root", "cooking");
    // si no ha podido conectar devuelve null, comprobamos
    if (!$conexion) {
        // Acabamos el programa dando msg de error
        die("No se ha podido establecer la conexión con el servidor");
    }
    // si todo ha ido ok devolvemos la conexión
    return $conexion;
}