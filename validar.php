<!DOCTYPE html>
<?php
// require e inicio de sesión al principio de todo para no olvidarnos
session_start();
require_once 'bbdd.php';
?>

<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // recogemos las variables del login del POST
        $chef = $_POST["chef"];
        $code = $_POST["code"];
        if (comprobacion($chef, $code)) {
            // Guardamos en la variable de sesión el id del $chef que se ha logueado
            $_SESSION["chef"] = $chef;
            $categoria = getCategoryById($chef);
            $_SESSION["category"] = $categoria;
            if ($categoria == "Jefe") {
                // Mandamos al patrón a su homepage
                header("Location: homeboss.php");
            } else {
                // Mandamos al usuario a su homepage
                header("Location: homechef.php");
            }
        } else {
            echo "Usuario o contraseña incorrecta";
        }
        ?>
    </body>
</html>
